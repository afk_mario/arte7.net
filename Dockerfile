FROM python:3.7-alpine
LABEL maintainer="afk@ellugar.co"

# The enviroment variable ensures that the python output is set straight
# to the terminal with out buffering it first
ENV PYTHONUNBUFFERED 1


RUN mkdir /web
WORKDIR /web

RUN pip install pipenv
COPY ./src/Pipfile Pipfile
COPY ./src/Pipfile.lock Pipfile.lock

RUN apk add --no-cache --virtual .build-deps \
        gcc \
        git \
        jpeg-dev \
        libc-dev \
        linux-headers \
        make \
        musl-dev \
        pcre-dev \
        postgresql-dev \
        zlib-dev 

RUN pipenv install --system --deploy --ignore-pipfile
# RUN apk del .build-deps

COPY ./src /web

RUN python manage.py makemigrations
RUN python manage.py migrate
RUN python manage.py collectstatic --noinput

EXPOSE 8000

ENTRYPOINT ["gunicorn", "-b", ":8000", "arte7.wsgi:application"]
